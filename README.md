Listado de rutas de carpetas

```
OTIS-ADMIN-PANEL
|   .eslintrc.json
|   .gitignore
|   next-env.d.ts
|   next.config.js
|   package-lock.json
|   package.json
|   README.md
|   tsconfig.json
|   yarn.lock
|
+---node_modules
|
+---public
|       next.svg
|       vercel.svg
|
\---src
    +---app
    |       favicon.ico
    |       globals.css
    |       layout.tsx
    |       page.module.css
    |       page.tsx
    |
    +---components
    +---Icons
    +---Layouts
    \---Themes

```

## aceptar las configuraciones locales:

1. precionar `ctrl + shift + p`
2. Buscar `"TypeScript: Select TypeScript Version"`
3. Seleccionar `"Use Workspace Version"`

